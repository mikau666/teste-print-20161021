<html>
    <head>
        <title>Teste print</title>
        <style>
            em{
                font-weight: bold;
                font-style: normal;
            }
        </style>
    </head>
    <body>
        <form>
            <div class="container-input">
                <label for="valor">Valor do saque:</label>
                <input type="number" id="valor" name="valor" required>
            </div>
            <div class="container-input">
                <button type="button" id="botao">Sacar</button>
            </div>
        </form>
        <div>
            <p>Retorno: <em id="retorno">...</em></p>
            <span>Notas:</span>
            <p>R$ 100: <em id="nota-100">0</em></p>
            <p>R$ 50: <em id="nota-50">0</em></p>
            <p>R$ 20: <em id="nota-20">0</em></p>
            <p>R$ 10: <em id="nota-10">0</em></p>
            <p>Desconsiderando o valor quebrado de: <em id="nota-desconsiderado">0</em></p>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>
            var input_valor = document.getElementById('valor');
            var botao = document.getElementById('botao');
            var aviso = document.getElementById('retorno');

            var enviarForm = function()
            {
                var config_ajax = { };
                config_ajax.url =  'http://localhost/teste-print/sacar-client.php';
                config_ajax.type = 'POST';
                config_ajax.data = { 'valor' : input_valor.value };
                config_ajax.success = function(retorno)
                {
                    try { var objeto_retorno = $.parseJSON(retorno); }
                    catch(err) { var objeto_retorno = { status: false } }

                    if(!objeto_retorno.status) { $(aviso).html('nok'); }
                    else
                    {
                        $(aviso).html('ok');
                        $.each(objeto_retorno.notas , function(index , value){
                            $('#nota-'+index).html(value);
                        });
                    }
                };

                $.ajax(config_ajax);
            };

            botao.addEventListener('click' , enviarForm);
        </script>
    </body>
</html>