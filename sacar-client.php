<?php

//verificação do lado cliente
if(!isset($_POST['valor'])) { echo json_encode(array('status' => false)); exit; }

try
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://localhost/teste-print/sacar-server.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('valor' => (int)$_POST['valor'])));
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_VERBOSE, false);

    $retorno = curl_exec($ch);
}
catch(Exception $e) { echo json_encode(array('status' => false)); exit; }

echo $retorno;