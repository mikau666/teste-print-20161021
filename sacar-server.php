<?php

// verificação do lado servidor
if(!isset($_POST['valor'])) { echo json_encode(array('status' => false)); exit; }

echo json_encode(array('status' => true , 'notas' => calcularNotas($_POST['valor'])));

// função para calcular
function calcularNotas($valor = false)
{
    $desconsiderado = ($valor % 10);

    $todas_notas = array(
        100 => 0,
        50 => 0,
        20 => 0,
        10 => 0
    );

    if($valor == false || $valor <= 0 || empty($valor)) { return $todas_notas; }

    foreach($todas_notas as $nota => $qtde)
    {
        if(($valor - $nota) < 0) { continue; }
        while(($valor - $nota) >= 0)
        {
            $valor -= $nota;
            $todas_notas[$nota]++;
        }
    }

    $todas_notas['desconsiderado'] = $desconsiderado;

    return $todas_notas;
}